#imports
import time,codecs,sys
import serial

#some definitions:
#配置串口通信
ser = serial.Serial()
ser.baudrate = 115200   #波特率
ser.port = "COM5"

F=open('1.bin','wb')

try:
	ser.open()#打开端口
except:
	print('port open error!')

time.sleep(0.1) #小憩0.1秒

def bytes_to_int(bytes):
    result = 0
    for b in bytes:
        result = result * 256 + int(b)
    return result
    
while True:
	data = ser.readline()	#ser.readline()是读取一行数据 直到遇到\n
	F.write(data)
	F.flush()
	print(time.time(),' ',int(data[0]))
F.close()