#imports
import time,codecs,sys
import serial

#some definitions:
#配置串口通信
ser = serial.Serial()
ser.baudrate = 256000   #波特率
ser.port = "COM29"

F=codecs.open('1.csv','w','utf-8')  #存在覆盖危险

try:
    ser.open()#打开端口
except:
    print('port open error!')
    sys.exit()

time.sleep(0.1) #小憩0.1秒
print("Now listening")

data = ser.readline()
if data.decode("utf-8").strip().startswith("STM32_OSC_2020 V"):
    print(data)

def bytes_to_int(bytes):
    result = 0
    for b in bytes:
        result = result * 256 + int(b)
    return result
    
while True:
    data = ser.read(500)	#ser.readline()是读取一行数据 直到遇到\n
    tnow=time.time()
    for d in data:
        dint=int(d)
        F.write("%d,%f\r\n" % (dint,tnow))
    F.flush()
    print(tnow,' ',int(data[0]))
F.close()