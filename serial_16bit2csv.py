#imports
import time,codecs,sys
import serial

#some definitions:
#配置串口通信
ser = serial.Serial()
ser.baudrate = 256000   #波特率
ser.port = "COM5"

F=codecs.open('1.csv','w','utf-8')  #存在覆盖危险

try:
    ser.open()#打开端口
except:
    print('port open error!')
    sys.exit()

time.sleep(0.1) #小憩0.1秒
print("Now listening")

while 1:
    data=ser.read(1)
    #print(data)
    if data!=b'':
        break

if data==b'S':
    data=ser.readline()
    if data.decode("utf-8").strip().startswith("TM32_OSC_2020 V"):
        print(b'S'+data)

def bytes_to_int(bytes):
    result = 0
    for b in bytes:
        result = result * 256 + int(b)
    return result

t0=time.time()  
tlast=t0

while True:
    data = ser.read(1000)	#ser.readline()是读取一行数据 直到遇到\n
    tnow=time.time()
    ldata=len(data)
    for n in range(int(ldata/2)):
        dint=bytes_to_int(data[n*2:n*2+2])
        F.write("%d,%f\r\n" % (dint,tnow))
    F.flush()
    #print(tnow,' ',bytes_to_int(data[0:2]),' ',ldata/2/(tnow-tlast)/1000,' KHz')
    tderta=tnow-t0
    dsample=bytes_to_int(data[0:2])
    try:
        freq=ldata/2/(tnow-tlast)/1000  #/2 because of 16bit
    except:
        freq=1
    print("{} {} {:.2f}KHz".format(tderta,dsample,freq))
    tlast=tnow
F.close()